# README.md for Sublime Text Resources

The resources directory contains data files and artifacts associated with blog posts.

Each section describes a file or file type.


## [`*.sublime-settings`](resources-sublime/Anaconda.sublime-settings)

The Sublime Text 3 settings are typically found in `$HOME/.config/sublime-text-3/Packages/{PACKAGE_NAME}/`

### [Anaconda.sublime-settings](resources-sublime/Anaconda.sublime-settings)

json setting file for Sublime Text 3 autolinting plugin named "Anaconda" (no relation to the Anaconda python package manager from Anaconda Inc)

To install this file, move it to `$HOME/.config/sublime-text-3/Packages/User/`.

Alternatively you can copy the contents of this file and paste it into the text file under the Sublime Text menu Preferences -> Package Settings -> Anaconda .

### [SublimeLinter.sublime-settings](resources-sublime/SublimeLinter.sublime-settings)

json setting file for Sublime Text 3 autolinting plugin named "Anaconda" (no relation to the Anaconda python package manager from Anaconda Inc)

To install this file, move it to `$HOME/.config/sublime-text-3/Packages/User/`

Alternatively you can copy the contents of this file and paste it into the text file under the Sublime Text menu Preferences -> Package Settings -> SublimeLinter .
